Source/Upstream: Yes, fixed in git branches and master
Reason: Fix build with x265 4.0

From 74b14ccd314aa091fa6217ebd4954489fbb7ccc8 Mon Sep 17 00:00:00 2001
From: Gyan Doshi <ffmpeg@gyani.pro>
Date: Sun, 11 Aug 2024 12:51:50 +0530
Subject: [PATCH] lavc/libx265: unbreak build for X265_BUILD >= 210

x265 added support for alpha starting with build 210.
While doing so, x265_encoder_encode() changed its fifth arg to
an array of pointers to x265_picture. This broke building lavc/libx265.c

This patch simply unbreaks the build and maintains existing single-layer
non-alpha encoding support.

Fixes #11130
---
 libavcodec/libx265.c | 42 +++++++++++++++++++++++++++++++-----------
 1 file changed, 31 insertions(+), 11 deletions(-)

diff --git a/libavcodec/libx265.c b/libavcodec/libx265.c
index b2008e96f1..8475539f0a 100644
--- a/libavcodec/libx265.c
+++ b/libavcodec/libx265.c
@@ -476,7 +476,13 @@ static int libx265_encode_frame(AVCodecContext *avctx, AVPacket *pkt,
 {
     libx265Context *ctx = avctx->priv_data;
     x265_picture x265pic;
-    x265_picture x265pic_out = { 0 };
+#if X265_BUILD >= 210
+    x265_picture x265pic_layers_out[MAX_SCALABLE_LAYERS];
+    x265_picture* x265pic_lyrptr_out[MAX_SCALABLE_LAYERS];
+#else
+    x265_picture x265pic_solo_out = { 0 };
+#endif
+    x265_picture* x265pic_out;
     x265_nal *nal;
     uint8_t *dst;
     int pict_type;
@@ -517,8 +523,16 @@ static int libx265_encode_frame(AVCodecContext *avctx, AVPacket *pkt,
         }
     }
 
+#if X265_BUILD >= 210
+    for (i = 0; i < MAX_SCALABLE_LAYERS; i++)
+        x265pic_lyrptr_out[i] = &x265pic_layers_out[i];
+
+    ret = ctx->api->encoder_encode(ctx->encoder, &nal, &nnal,
+                                   pic ? &x265pic : NULL, x265pic_lyrptr_out);
+#else
     ret = ctx->api->encoder_encode(ctx->encoder, &nal, &nnal,
-                                   pic ? &x265pic : NULL, &x265pic_out);
+                                   pic ? &x265pic : NULL, &x265pic_solo_out);
+#endif
 
     av_freep(&x265pic.quantOffsets);
 
@@ -546,10 +560,16 @@ static int libx265_encode_frame(AVCodecContext *avctx, AVPacket *pkt,
             pkt->flags |= AV_PKT_FLAG_KEY;
     }
 
-    pkt->pts = x265pic_out.pts;
-    pkt->dts = x265pic_out.dts;
+#if X265_BUILD >= 210
+    x265pic_out = x265pic_lyrptr_out[0];
+#else
+    x265pic_out = &x265pic_solo_out;
+#endif
+
+    pkt->pts = x265pic_out->pts;
+    pkt->dts = x265pic_out->dts;
 
-    switch (x265pic_out.sliceType) {
+    switch (x265pic_out->sliceType) {
     case X265_TYPE_IDR:
     case X265_TYPE_I:
         pict_type = AV_PICTURE_TYPE_I;
@@ -573,17 +593,17 @@ FF_ENABLE_DEPRECATION_WARNINGS
 #endif
 
 #if X265_BUILD >= 130
-    if (x265pic_out.sliceType == X265_TYPE_B)
+    if (x265pic_out->sliceType == X265_TYPE_B)
 #else
-    if (x265pic_out.frameData.sliceType == 'b')
+    if (x265pic_out->frameData.sliceType == 'b')
 #endif
         pkt->flags |= AV_PKT_FLAG_DISPOSABLE;
 
-    ff_side_data_set_encoder_stats(pkt, x265pic_out.frameData.qp * FF_QP2LAMBDA, NULL, 0, pict_type);
+    ff_side_data_set_encoder_stats(pkt, x265pic_out->frameData.qp * FF_QP2LAMBDA, NULL, 0, pict_type);
 
-    if (x265pic_out.userData) {
-        memcpy(&avctx->reordered_opaque, x265pic_out.userData, sizeof(avctx->reordered_opaque));
-        av_freep(&x265pic_out.userData);
+    if (x265pic_out->userData) {
+        memcpy(&avctx->reordered_opaque, x265pic_out->userData, sizeof(avctx->reordered_opaque));
+        av_freep(&x265pic_out->userData);
     } else
         avctx->reordered_opaque = 0;
 
-- 
2.25.1

