# Copyright 2016-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=Mopidy-Iris
MY_PNV=${MY_PN}-${PV}

require pypi [ pn=${MY_PN} pnv=${MY_PNV} ] \
    setup-py [ import=setuptools blacklist='2 3.6' multibuild=false ]

SUMMARY="Fully-featured Mopidy frontend client"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# tests run gstreamer and sudo
RESTRICT="test"

DEPENDENCIES="
    build+run:
        dev-python/Pykka[>=2.0.1][python_abis:*(-)?]
        media-sound/mopidy[>=3.0][python_abis:*(-)?]
    suggestion:
        app-admin/sudo [[
            description = [ Required to run a (re-)scan of local files ]
        ]]
        media-plugins/mopidy-local[python_abis:*(-)?] [[
            description = [ Mopidy extension for playing music from your local music archive ]
        ]]
        media-plugins/mopidy-spotify[python_abis:*(-)?] [[
            description = [ Mopidy extension for playing music from Spotify ]
        ]]
"

WORK=${WORKBASE}/${MY_PNV}

