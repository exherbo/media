# Copyright 2019-2022 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require meson \
    gsettings \
    gtk-icon-cache

SUMMARY="Tethered shooting and control of digital cameras"
DESCRIPTION="
Entangle is an application which uses GTK and libgphoto2 to provide a graphical interface for
tethered photography with digital cameras.

It includes control over camera shooting and configuration settings and 'hands off' shooting
directly from the controlling computer.
"
HOMEPAGE="https://${PN}-photo.org"
DOWNLOADS="${HOMEPAGE}/download/sources/${PNV}.tar.xz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gtk-doc
"

DEPENDENCIES="
    build:
        dev-doc/gtk-doc
        virtual/pkg-config
    build+run:
        dev-libs/gexiv2[>=0.10][gobject-introspection(+)]
        dev-libs/glib:2[>=2.38.0][gobject-introspection]
        dev-libs/libpeas:1.0[>=1.2.0]
        gnome-desktop/gobject-introspection:1[>=1.54.0]
        gnome-desktop/libgudev[>=145]
        media-libs/gstreamer:1.0[>=1.0.0][gobject-introspection]
        media-libs/lcms2[>=2.0]
        media-libs/libgphoto2[>=2.5.0]
        media-libs/libraw[>=0.9.0]
        media-plugins/gst-plugins-base:1.0[>=1.0.0]
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0[>=2.12.0]
        x11-libs/gtk+:3[>=3.22.0][gobject-introspection]
        x11-libs/libXext[>=1.3.0]
        x11-libs/pango[>=1.40.11]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/54795d275a93e94331a614c8712740fcedbdd4f0.patch
)

MESON_SRC_CONFIGURE_PARAMS=(
    -Denable-gtk-doc=true
    -Denable-werror=false
)

src_install() {
    meson_src_install

    ! option gtk-doc && edo rm -rf "${IMAGE}"/usr/share/gtk-doc
}

pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

