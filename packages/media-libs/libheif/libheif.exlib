# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=strukturag release=v${PV} suffix=tar.gz ]
require cmake
require ffmpeg [ with_opt=true ]
require option-renames [ renames=[ 'aom providers:aom' 'rav1e providers:rav1e' 'svt-av1 providers:svt-av1' ] ]

export_exlib_phases src_install

SUMMARY="libheif is an HEIF and AVIF file format decoder and encoder"
DESCRIPTION="
libheif is an ISO/IEC 23008-12:2017 HEIF and AVIF (AV1 Image File Format) file format decoder and
encoder. There is partial support for ISO/IEC 23008-12:2022 (2nd Edition) capabilities.

HEIF and AVIF are new image file formats employing HEVC (H.265) or AV1 image coding, respectively,
for the best compression ratios currently possible.

libheif makes use of libde265 for HEIF image decoding and x265 for encoding.
For AVIF, libaom, dav1d, svt-av1, or rav1e are used as codecs.
"

LICENCES="
    GPL-3  [[ note = [ sample applications ] ]]
    LGPL-3 [[ note = library ]]
"
SLOT="0"
MYOPTIONS="
    h264   [[ description = [ Support for H.264/AVC ] ]]
    ffmpeg [[ description = [ Support ffmpeg for decoding of HEIF images ] ]]

    ( providers: aom dav1d ) [[
        *description = [ Providers for decoding AVIF images ]
        number-selected = at-least-one
    ]]
    ( providers: aom rav1e svt-av1 ) [[
        *description = [ Providers for encoding AVIF images ]
        number-selected = at-least-one
    ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/libde265
        media-libs/libpng:=
        media-libs/tiff:=
        media-libs/x265:=
        sys-libs/zlib
        h264? ( media-libs/openh264 )
        providers:aom? ( media-libs/aom:= )
        providers:dav1d? ( media-libs/dav1d:= )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:rav1e? ( media-video/rav1e )
        providers:svt-av1? ( media-libs/SVT-AV1:= )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_FIND_ROOT_PATH:PATH="$(ffmpeg_alternatives_prefix);/usr/$(exhost --target)"
    -DCMAKE_COMPILE_WARNING_AS_ERROR:BOOL=FALSE
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DENABLE_COVERAGE:BOOL=FALSE
    -DENABLE_MULTITHREADING_SUPPORT:BOOL=TRUE
    -DWITH_EXAMPLES:BOOL=TRUE
    -DWITH_FUZZERS:BOOL=OFF
    -DWITH_GDK_PIXBUF:BOOL=FALSE
    -DWITH_HEADER_COMPRESSION:BOOL=FALSE
    -DWITH_UNCOMPRESSED_CODEC:BOOL=FALSE
    # Not sure we need JPEG support here, although the dependency is already
    # used for heif-enc and heif-convert
    -DWITH_JPEG_DECODER:BOOL=FALSE
    -DWITH_JPEG_ENCODER:BOOL=FALSE
    # Unwritten
    -DWITH_KVAZAAR:BOOL=FALSE
    -DWITH_OPENJPH_ENCODER:BOOL=FALSE
    # Unwritten and Experimental
    -DWITH_UVG266:BOOL=FALSE
    -DWITH_VVDEC:BOOL=FALSE
    -DWITH_VVENC:BOOL=FALSE
    # decoder for HEIF
    -DWITH_LIBDE265:BOOL=TRUE
    -DWITH_LIBSHARPYUV:BOOL=FALSE
    -DWITH_OpenJPEG_ENCODER:BOOL=FALSE
    -DWITH_OpenJPEG_DECODER:BOOL=FALSE
    # encoder for HEIF
    -DWITH_X265:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    # en-/decoder for AVIF
    'providers:aom AOM_DECODER'
    'providers:aom AOM_ENCODER'
    # decoder for AVIF
    'providers:dav1d DAV1D'
    # decoder for HEIF
    'ffmpeg FFMPEG_DECODER'
    # en-/decoder for AVC
    'h264 OpenH264_ENCODER'
    'h264 OpenH264_DECODER'
    # encoder for AVIF
    'providers:rav1e RAV1E'
    'providers:svt-av1 SvtEnc'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
    # A bit unfortunate, but "Tests can only be compiled with full symbol
    # visibility"
    '-DWITH_REDUCED_VISIBILITY:BOOL=FALSE -DWITH_REDUCED_VISIBILITY:BOOL=TRUE'
)

CMAKE_SRC_TEST_PARAMS=(
    # Seems to need -DWITH_UNCOMPRESSED_CODEC=TRUE to pass
    -E region
)

libheif_src_install() {
    cmake_src_install

    # libheif expects this, even if empty
    keepdir /usr/$(exhost --target)/lib/libheif
}

