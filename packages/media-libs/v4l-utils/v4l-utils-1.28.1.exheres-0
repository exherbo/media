# Copyright 2009-2010 Ingmar Vanhassel
# Copyright 2011 Daniel Mierswa <impulze@impulze.org>
# Copyright 2012-2025 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require meson [ cross_prefix=true ]
require freedesktop-desktop gtk-icon-cache udev-rules systemd-service

SUMMARY="Collection of libraries that add a thin abstraction layer on top of video4linux2 (V4L2) devices"
DESCRIPTION="
libv4l is a collection of libraries that adds a thin abstraction layer on top
of video4linux2 (V4L2) devices. The purpose of this layer is to make it easy
for application writers to support a wide variety of devices without having to
write separate code for different devices in the same class. It consists of 3
different libraries. libv4lconvert offers functions to convert from any (known)
pixel format to V4l2_PIX_FMT_BGR24 or V4l2_PIX_FMT_YUV420. libv4l1 offers the
(deprecated) v4l1 API on top of v4l2 devices, independent of the drivers for
those devices supporting v4l1 compatibility (which many v4l2 drivers do not).
libv4l2 offers the v4l2 API on top of v4l2 devices, while adding support for
the application transparent libv4lconvert conversion where necessary.
"
HOMEPAGE="https://www.linuxtv.org"
DOWNLOADS="${HOMEPAGE}/downloads/${PN}/${PNV}.tar.xz"

LICENCES="GPL-2 || ( LGPL-2.1 LGPL-3 )"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    qt6 [[ description = [ Build the Qt-based GUI applications qv4l2 and qvidcap ] ]]

    ( libc: musl )
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        qt6? (
            dev-libs/libglvnd
            sys-sound/alsa-lib
            x11-libs/qt5compat:6
            x11-libs/qtbase:6[gui]
        )
        libc:musl? ( dev-libs/argp-standalone )
        providers:eudev? ( sys-apps/eudev )
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=6] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:systemd? ( sys-apps/systemd )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dbpf=disabled
    -Ddocdir=/usr/share/doc/${PNVR}
    -Ddoxygen-doc=disabled
    -Ddoxygen-html=false
    -Ddoxygen-man=false
    -Dgconv=disabled
    -Dgconvsysdir=/usr/$(exhost --target)/lib/gconv
    -Djpeg=enabled
    -Dlibdvbv5=enabled
    -Dsystemdsystemunitdir="${SYSTEMDSYSTEMUNITDIR}"
    -Dudevdir="${UDEVDIR}"
    -Dv4l-plugins=true
    -Dv4l-utils=true
    -Dv4l-wrappers=true
    -Dv4l2-compliance-32=false
    -Dv4l2-compliance-libv4l=true
    -Dv4l2-ctl-32=false
    -Dv4l2-ctl-libv4l=true
    -Dv4l2-ctl-stream-to=true
    -Dv4l2-tracer=disabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'qt6 qv4l2'
    'qt6 qvidcap'
)

src_prepare() {
    meson_src_prepare

    # fix install
    edo sed \
        -e '/sbin/d' \
        -i utils/v4l2-dbg/meson.build
}

src_install() {
    meson_src_install

    # remove empty directories
    edo rm -rf "${IMAGE}"/etc/rc_keymaps
}

pkg_postinst() {
    option qt6 && freedesktop-desktop_pkg_postinst
    option qt6 && gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    option qt6 && freedesktop-desktop_pkg_postrm
    option qt6 && gtk-icon-cache_pkg_postrm
}

