# Copyright 2009 Maxime Coste <frrrwww@gmail.com>
# Distributed under the terms of the GNU General Public License v2

MY_PNV=${PNV/SDL/SDL2}

require github [ user=libsdl-org project=${PN} release=release-${PV} suffix=tar.gz ]
require cmake

SUMMARY="Image decoding for many popular formats for Simple Directmedia Layer"

LICENCES="ZLIB"
SLOT="$(ever range 1)"
PLATFORMS="~amd64 ~armv7 ~x86"
MYOPTIONS="
    avif [[ description = [ Support for the AV1 Image File Format ] ]]
    jpegxl
    tiff
    webp
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        media-libs/SDL:2[>=2.0.9]
        media-libs/libpng:=
        sys-libs/zlib
        avif? ( media-libs/libavif:=[>=0.9.3] )
        jpegxl? ( media-libs/libjxl:= )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        tiff? ( media-libs/tiff:= )
        webp? ( media-libs/libwebp:= )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DSDL2IMAGE_BACKEND_STB:BOOL=FALSE
    # link to the graphics libraries instead of dlopening them at runtime
    -DSDL2IMAGE_DEPS_SHARED:BOOL=FALSE
    -DSDL2IMAGE_STRICT:BOOL=TRUE
    -DSDL2IMAGE_VENDORED:BOOL=FALSE

    -DSDL2IMAGE_JPG:BOOL=TRUE
    -DSDL2IMAGE_PNG:BOOL=TRUE
    -DSDL2IMAGE_SVG:BOOL=TRUE
    -DSDL2IMAGE_ZLIB:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'avif SDL2IMAGE_AVIF'
    'jpegxl SDL2IMAGE_JXL'
    'tiff SDL2IMAGE_TIF'
    'webp SDL2IMAGE_WEBP'
)

