# Copyright 2010 Timothy Redaelli <timothy@redaelli.eu>
# Copyright 2012-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=v${PV} ] \
    cmake \
    python [ blacklist=2 multibuild=false ] \
    toolchain-funcs

SUMMARY="A library for rectifying and simulating photographic lens distortions"

LICENCES="LGPL-3 CCPL-Attribution-ShareAlike-3.0"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    doc
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? (
            app-doc/doxygen
            dev-python/docutils [[ note = [ rst2man ] ]]
        )
    build+run:
        dev-libs/glib:2[>=2.40]
        media-libs/libpng:=
        sys-libs/zlib
"

src_prepare() {
    cmake_src_prepare

    # fix shebangs
    edo sed \
        -e "s:#!/usr/bin/env python3:#!/usr/bin/env python$(python_get_abi):g" \
        -i apps/lensfun-{add-adapter,update-data}

    # required for setuptools easy_install to work, else pth file doesn't get installed
    export PYTHONPATH="${IMAGE}"/$(python_get_libdir)/site-packages/
}

src_configure() {
    local cmakeargs=(
        -DBUILD_AUXFUN:BOOL=TRUE
        -DBUILD_LENSTOOL:BOOL=TRUE
        -DBUILD_STATIC:BOOL=FALSE
        -DINSTALL_HELPER_SCRIPTS:BOOL=TRUE
        -DINSTALL_PYTHON_MODULE:BOOL=TRUE
        -DPYTHON:PATH=${PYTHON}
        $(cmake_build DOC)
        $(expecting_tests -DBUILD_TESTS:BOOL=TRUE -DBUILD_TESTS:BOOL=FALSE)
        $(cc-has-defined __SSE__ && echo -DBUILD_FOR_SSE:BOOL=TRUE || echo -DBUILD_FOR_SSE:BOOL=FALSE)
        $(cc-has-defined __SSE2__ && echo -DBUILD_FOR_SSE2:BOOL=TRUE || echo -DBUILD_FOR_SSE2:BOOL=FALSE)
    )

    ecmake "${cmakeargs[@]}"
}

src_install() {
    cmake_src_install

    # used by the lensfun-update-data tool
    keepdir /var/lib/lensfun-updates
}

