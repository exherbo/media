# Copyright 2008 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN}core

SUMMARY="XviD, a high performance/quality MPEG-4 video de-/encoding solution"
DESCRIPTION="
The Xvid video codec implements MPEG-4 Simple Profile and Advanced
Simple Profile standards. It permits compressing and decompressing digital video
in order to reduce the required bandwidth of video data for transmission over
computer networks or efficient storage on CDs or DVDs. Due to its unrivalled
quality Xvid has gained great popularity and is used in many other GPLed
applications, like e.g. Transcode, MEncoder, MPlayer, Xine and many more.
"
HOMEPAGE="https://www.${PN}.com"
DOWNLOADS="https://downloads.${PN}.com/downloads/${MY_PN}-${PV}.tar.bz2"

REMOTE_IDS="freecode:${PN}"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    debug
    platform: amd64 x86
"

DEPENDENCIES="
    build:
        platform:amd64? ( dev-lang/yasm[>=0.8.0] )
        platform:x86? ( dev-lang/yasm[>=0.8.0] )
"

WORK="${WORKBASE}"/${MY_PN}/build/generic

DEFAULT_SRC_CONFIGURE_PARAMS=( --enable-pthread )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( "debug idebug" )

DEFAULT_SRC_INSTALL_EXTRA_PREFIXES=( "${WORKBASE}/${MY_PN}/" )

install_one_multibuild() {
    local host=$(exhost --target)

    default

    local mylib=$(ls "${IMAGE}"/usr/${host}/lib/libxvidcore.so.*)
    mylib=${mylib##*/}
    dosym ${mylib} /usr/${host}/lib/libxvidcore.so
    dosym ${mylib} /usr/${host}/lib/${mylib%.?}

    edo chmod 755 "${IMAGE}"/usr/${host}/lib/${mylib}
}

