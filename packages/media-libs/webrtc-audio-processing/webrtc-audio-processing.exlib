# Copyright 2012 Niels Ole Salscheider <niels_ole@salscheider-online.de>
# Distributed under the terms of the GNU General Public License v2

require meson toolchain-funcs

export_exlib_phases src_configure

SUMMARY="This is a copy of the AudioProcessing module from the WebRTC project"
HOMEPAGE="https://freedesktop.org/software/pulseaudio/webrtc-audio-processing"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.xz"

LICENCES="BSD-3"
SLOT="$(ever major)"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-cpp/abseil
"

webrtc-audio-processing_src_configure() {
    local meson_args=(
        -Dgnustl=disabled
    )

    if ever at_least 2.1 ; then
        meson_args+=(
            $(cc-macro-is-true "defined(__ARM_NEON)" && echo -Dneon=enabled || echo -Dneon=disabled)
            $(cc-has-defined __SSE__ && echo -Dinline-sse=true || echo -Dinline-sse=false)
        )
    else
        meson_args+=(
            $(cc-macro-is-true "defined(__ARM_NEON)" && echo -Dneon=yes || echo -Dneon=no)
        )
    fi

    exmeson "${meson_args[@]}"
}

