# Copyright 2020-2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake
require alternatives

export_exlib_phases src_configure src_test src_install

SUMMARY="Reference implementation for AV1, an open, royalty-free video coding format"

HOMEPAGE="https://aomedia.org/"
DOWNLOADS="https://storage.googleapis.com/aom-releases/lib${PNV}.tar.gz"

LICENCES="BSD-2"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        dev-lang/yasm
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    test:
        dev-lang/python:*
    run:
        !media-libs/aom:0[<2.0.0-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

if ever at_least 3.8.0 ; then
    DEPENDENCIES+="
        build:
            doc? ( app-doc/doxygen[>=1.8.10] )
    "
    CMAKE_SOURCE="${WORKBASE}/lib${PNV}"
else
    CMAKE_SOURCE="${WORKBASE}"
fi

UPSTREAM_CHANGELOG="https://aomedia.googlesource.com/aom/+/refs/heads/master/CHANGELOG"

# Needs to download external test data
RESTRICT="test"

aom_src_configure() {
    local cmake_params+=(
        -DBUILD_SHARED_LIBS=1
        # requires wxwidgets (wxgtk)
        -DCONFIG_ANALYZER=0
        -DCONFIG_AV1_DECODER=1
        -DCONFIG_AV1_ENCODER=1
        -DCONFIG_MULTITHREAD=1
        -DCONFIG_QUANT_MATRIX=1
        # Unwritten, https://github.com/Netflix/vmaf
        -DCONFIG_TUNE_VMAF=0
        -DENABLE_CCACHE=0
        -DENABLE_DISTCC=0
        -DENABLE_EXAMPLES=0
        # Prefer yasm over nasm
        -DENABLE_NASM=0
        -DENABLE_WERROR=0

        $(cmake_enable doc DOCS)
        $(expecting_tests '-DENABLE_TESTS:BOOL=TRUE -DENABLE_TESTS:BOOL=FALSE')
    )

    if ever at_least 3.8.0 ; then
        cmake_params+=(
            # libjxl >= 0.9.0 removed the butteraugli api
            -DCONFIG_TUNE_BUTTERAUGLI=0
        )
    fi

    ecmake "${cmake_params[@]}"
}

aom_src_test() {
    emake runtests
}

aom_src_install() {
    local arch_dependent_alternatives=()
    local host=$(exhost --target)

    default

    if option doc ; then
        docinto html
        dodoc -r "${WORK}"/docs/html/*
    fi

    arch_dependent_alternatives+=(
        /usr/${host}/include/${PN}          ${PN}-${SLOT}
        /usr/${host}/lib/lib${PN}.a         lib${PN}-${SLOT}.a
        /usr/${host}/lib/lib${PN}.so        lib${PN}-${SLOT}.so
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

