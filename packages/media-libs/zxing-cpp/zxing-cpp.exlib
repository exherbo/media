# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user="zxing-cpp" tag=v${PV} ] cmake
require alternatives

export_exlib_phases src_install

SUMMARY="C++ port of the ZXing barcode scanning library"
DESCRIPTION="
- In pure C++11, no third-party dependencies
- Stateless, thread-safe readers/generators

Same as with ZXing, the  following barcode are supported:
1D product | 1D industrial | 2D
---------------------------------------------------
UPC-A      | Code 39       | QR Code
UPC-E      | Code 93       | Data Matrix
EAN-8      | Code 128      | Aztec (beta)
EAN-13     | Codabar       | PDF 417 (beta)
           | ITF
           | RSS-14
           | RSS-Expanded
"

LICENCES="
    Apache-2.0
    LGPL-2.1 [[ note = [ Part imported from Qt ] ]]
"
MYOPTIONS="examples"

DEPENDENCIES="
    build+run:
        media-libs/zint
        examples? (
            dev-libs/stb
            media-libs/opencv
            x11-libs/qtbase:6
            x11-libs/qtdeclarative:6
            x11-libs/qtmultimedia:6
        )
    test:
        dev-libs/fmt
        dev-libs/stb
    run:
        !media-libs/zxing-cpp[<1.2.0-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DBUILD_SHARED_LIBS=TRUE
    -DZXING_USE_BUNDLED_ZINT:BOOL=FALSE
    -DZXING_C_API:BOOL=FALSE
    -DZXING_EXPERIMENTAL_API:BOOL=FALSE
    -DZXING_PYTHON_MODULE:BOOL=FALSE
    -DZXING_READERS:BOOL=TRUE
    -DZXING_WRITERS:STRING=ON
)
CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'examples ZXING_EXAMPLES'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DZXING_BLACKBOX_TESTS:BOOL=TRUE -DZXING_BLACKBOX_TESTS:BOOL=FALSE'
    '-DZXING_UNIT_TESTS:BOOL=TRUE -DZXING_UNIT_TESTS:BOOL=FALSE'
)

# samples/aztec-1 is not part of the tarball
CMAKE_SRC_TEST_PARAMS+=( -E ReaderTest )

zxing-cpp_src_install() {
    local arch_dependent_alternatives=()
    local host=$(exhost --target)

    cmake_src_install

    arch_dependent_alternatives+=(
        /usr/${host}/include/ZXing          ZXing-${SLOT}
        /usr/${host}/lib/libZXing.so        libZXing-${SLOT}.so
        /usr/${host}/lib/cmake/ZXing/ZXingConfig.cmake        zxing-${SLOT}-cmakeConfig.cmake
        /usr/${host}/lib/cmake/ZXing/ZXingConfigVersion.cmake zxing-${SLOT}-cmakeConfigVersion.cmake
        /usr/${host}/lib/cmake/ZXing/ZXingTargets.cmake       zxing-${SLOT}-cmakeTargets.cmake
        /usr/${host}/lib/cmake/ZXing/ZXingTargets-none.cmake  zxing-${SLOT}-cmakeTargets-none.cmake
        /usr/${host}/lib/pkgconfig/zxing.pc zxing-${SLOT}.pc
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"

    edo pushd "${WORKBASE}/${PNV}"
    emagicdocs
    edo popd
}

