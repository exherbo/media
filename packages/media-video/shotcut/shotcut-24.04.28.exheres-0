# Copyright 2014-2024 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=mltframework tag=v${PV} ] \
    cmake \
    ffmpeg [ options="[X][h264][hevc][mp3][opus][pulseaudio][theora][vpx]" min_versions=[ 4.4 ] ] \
    freedesktop-desktop \
    gtk-icon-cache

SUMMARY="Shotcut is a free, open source, cross-platform video editor"
HOMEPAGE+=" https://www.shotcut.org"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

QT_MIN_VER="6.4"

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-libs/qttools:6[>=${QT_MIN_VER}] [[ note = [ lrelease/lconvert for translations ] ]]
    build+run:
        media/mlt:7[>=7.22.0][ffmpeg][frei0r][glaxnimate][qt6][ffmpeg_abis:*(-)?]
        sci-libs/fftw[>=3]
        x11-libs/qtbase:6[>=${QT_MIN_VER}][gui][sql]
        x11-libs/qtdeclarative:6[>=${QT_MIN_VER}]
        x11-libs/qtmultimedia:6[>=${QT_MIN_VER}]
    run:
        media-libs/SDL:2
        media-sound/jack-audio-connection-kit
    suggestion:
        media-gfx/glaxnimate [[ description = [ Required for creating and editing vector animations ] ]]
"

src_prepare() {
    cmake_src_prepare

    # TODO: fix upstream
    # code does some weird dir.cdUp and dir.cd actions
    edo sed \
        -e 's:appPath = applicationDirPath():appPath = "/usr/bin":g' \
        -i src/main.cpp
    edo sed \
        -e 's;QCoreApplication::applicationDirPath();"/usr/bin";g' \
        -i src/mltxmlchecker.cpp
    edo sed \
        -e 's:qApp->applicationDirPath():"/usr/bin":g' \
        -i src/qmltypes/qmlapplication.cpp
}

src_install() {
    cmake_src_install

    # remove empty directory
    edo rm -rf "${IMAGE}"/usr/share/shotcut/translations/.lupdate
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

